import java.awt.Color;
import java.util.Random;

public class KukuColor {
	
	public static Color jelenlegi;
	public static Color eltero;
	private static Random randomGenerator = new Random();
	
	public static Color getColor(int elteres){
		int halmaz = 255-elteres;
		int r = randomGenerator.nextInt(halmaz);
		int g = randomGenerator.nextInt(halmaz);
		int b = randomGenerator.nextInt(halmaz);
		
		jelenlegi = new Color(r,g,b);
		eltero = new Color(r+elteres, g+elteres, b+elteres);
		return jelenlegi;
	}
	
	private KukuColor() {
		
	}
	
}
