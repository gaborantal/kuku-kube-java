import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

class Gui extends JFrame{

	//Belso osztaly START
	private class Szamlalo extends Thread{
		
		public void run(){
			for (int i = Settings.JATEKIDO; i >= 0; i--) {
				try{
					Thread.sleep(1000);
					System.out.println(i);
					setIdoLabel(i);
				}catch(InterruptedException e){
					e.printStackTrace();
				}
			}
			JOptionPane.showMessageDialog(Gui.this, "J�t�k v�ge! Gratul�lunk, az el�rt pontod: " + pontszam);
			System.exit(0);
		}
	}
	//Belso osztaly STOP
	
	private static final long serialVersionUID = 3635022511137836897L;
	private JPanel pane = new JPanel();
	private JButton[][] negyzetek;
	
	private int nehezseg = Settings.NEHEZSEG;
	private int jelenlegiMeret = Settings.KEZDOMERET;
	private int korszamlalo = 0;
	private int pontszam = 0;
	
	private JLabel lblHatralevoIdo = new JLabel("H�tral�v� id�: "
			+ Settings.JATEKIDO + " m�sodperc");
	private JLabel lblPontszam = new JLabel("Pontsz�m: " + pontszam);
	
	public Gui(){
		negyzetek = new JButton[Settings.MAXMERET][Settings.MAXMERET];
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setPreferredSize(new Dimension(500,600));
		this.setResizable(false);
	}

	public void runGui() {
		palyaFeltolt();
		Szamlalo counter = new Szamlalo();
		counter.start();
	}

	private void palyaFeltolt() {
		this.getContentPane().removeAll();
		this.repaint();
		pane.removeAll();
		
		this.setLayout(new FlowLayout());
		
		pane.setPreferredSize(new Dimension(500,500));
		pane.setLayout(new GridLayout(jelenlegiMeret, jelenlegiMeret, 2, 2));
		
		Color szin = KukuColor.getColor(nehezseg);

		for (int i = 0; i < jelenlegiMeret; i++) {
			for (int j = 0; j < jelenlegiMeret; j++) {
				negyzetek[i][j] = new JButton();
				negyzetek[i][j].setBackground(szin);
				//optikai tuning
				negyzetek[i][j].setFocusPainted(false);;
				negyzetek[i][j].setBorder(BorderFactory.createEmptyBorder());
				pane.add(negyzetek[i][j]);
			}
		}
		randomMasSzin();
		
		this.add(pane);
		
		GridLayout tmp = new GridLayout(2,1);
		JPanel infoPanel = new JPanel(tmp);
		infoPanel.add(lblHatralevoIdo);
		infoPanel.add(lblPontszam);
		
		this.add(infoPanel);
		this.pack();
		this.setVisible(true);
		
		if(jelenlegiMeret < Settings.MAXMERET){
			if(korszamlalo+1 < 3){
				korszamlalo++;
			} else {
				korszamlalo = 0;
				jelenlegiMeret++;
			}
		}
	}

	private void randomMasSzin() {
		Random gen = new Random();
		int x,y;
		x = gen.nextInt(jelenlegiMeret);
		y = gen.nextInt(jelenlegiMeret);
		
		negyzetek[x][y].setBackground(KukuColor.eltero);
		negyzetek[x][y].addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				pontszam += Settings.PONTSZAMJO;
				setPontszamLabel();
				nehezseg = (nehezseg-Settings.LEPESKOZ) > Settings.MINKUL  
										? nehezseg-Settings.LEPESKOZ 
										: Settings.MINKUL;				
				palyaFeltolt();
				
			}
		});
	}

	protected void setPontszamLabel() {
		lblPontszam.setText("Pontsz�m: "+ pontszam);
	}
	
	protected void setIdoLabel(int par) {
		lblHatralevoIdo.setText("H�tral�v� id�: " + par + " m�sodperc");
	}
	

}
public class Main {
	public static void main(String[] args) {
		Gui g = new Gui();
		g.runGui();
	}
}
