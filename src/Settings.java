import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Settings {
	private static final Properties prop = new Properties();
	
	static{
		InputStream file = null;
		try{
			file = new FileInputStream("config.properties");
			prop.load(file);
		}catch(IOException e){
			e.printStackTrace();
		}finally{
			try {
				file.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private static int getProp(String id){
		int visszaTeres = 0;
		try{
			visszaTeres = Integer.parseInt(prop.getProperty(id));
		}catch(NumberFormatException e){
			e.printStackTrace();
		}
		return visszaTeres;
	}
	
	public static final int NEHEZSEG = getProp("nehezseg");
	public static final int KEZDOMERET = getProp("kezdo.meret");
	public static final int MAXMERET = getProp("max.meret");
	public static final int JATEKIDO = getProp("jatekido");
	public static final int PONTSZAMJO = getProp("pontszam.jo");
	public static final int LEPESKOZ = getProp("lepes.koz");
	public static final int MINKUL = getProp("min.kul");
	
	private Settings(){
	}
}